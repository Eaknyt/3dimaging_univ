#ifndef H_EPW3DIMAG_GLOBAL_H
#define H_EPW3DIMAG_GLOBAL_H

#include <cstdint>

namespace epw3dimag {

using uint16 = std::uint16_t;
using uint32 = std::uint32_t;

} // namespace epw3dimag

#endif // H_EPWDIP3DIMAG_GLOBAL_H
