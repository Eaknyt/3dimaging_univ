#include "volumeimage.h"

#include <assert.h>
#include <fstream>
#include <iostream>


namespace {

epw3dimag::uint16 swapEndian(epw3dimag::uint16 value)
{
    return (value >> 8) | (value << 8);
}

} // anon namespace


namespace epw3dimag {

VolumeImage::VolumeImage() :
    m_width(0),
    m_height(0),
    m_depth(0),
    m_count(0),
    m_data()
{}

VolumeImage::VolumeImage(uint32 w, uint32 h, uint32 d) :
    m_width(w),
    m_height(h),
    m_depth(d),
    m_count(w * h * d),
    m_data(m_count)
{
    clear(0);
}

VolumeImage::VolumeImage(const std::string &filename,
                         uint32 w, uint32 h, uint32 d) :
    m_width(w),
    m_height(h),
    m_depth(d),
    m_count(w * h * d),
    m_data(m_count)
{
    read(filename, w, h, d);
}

VolumeImage::~VolumeImage()
{}

std::vector<uint16> VolumeImage::data() const
{
    return m_data;
}

uint32 VolumeImage::width() const
{
    return m_width;
}
uint32 VolumeImage::height() const
{
    return m_height;
}

uint32 VolumeImage::depth() const
{
    return m_depth;
}

bool VolumeImage::isNull() const
{
    return (m_count == 0);
}

bool VolumeImage::isValid() const
{
    return (m_count != 0);
}

uint32 VolumeImage::voxelCount() const
{
    return m_count;
}

bool VolumeImage::read(const std::string &filename,
                       uint32 w, uint32 h, uint32 d)
{
    std::ifstream ifs(filename, std::ios::binary);

    if (ifs.fail()) {
        return false;
    }

    for (uint32 z = 0; z < d; ++z) {
        for (uint32 y = 0; y < h; ++y) {
            for (uint32 x = 0; x < w; ++x) {
                uint16 value;

                ifs.read((char *) (&value), sizeof(uint16));

                value = swapEndian(value);

                m_data.push_back(value);

                if (ifs.eof()) {
                    return false;
                }
            }
        }
    }

    ifs.close();

    m_width = w;
    m_height = h;
    m_depth = d;

    return true;
}

bool VolumeImage::write(const std::string &filename) const
{
    std::ofstream ofs(filename, std::ios::binary);

    if (ofs.fail()) {
        return false;
    }

    for (uint32 z = 0; z < m_depth; ++z) {
        for (uint32 y = 0; y < m_height; ++y) {
            for (uint32 x = 0; x < m_width; ++x) {
                uint16 valueToWrite = operator()(x, y, z);
                valueToWrite = swapEndian(valueToWrite);

                // /!\ Crashes
//                ofs.write(reinterpret_cast<const char *>(valueToWrite),
//                          sizeof(uint16));
            }
        }
    }

    // /!\ Works
    ofs.write(reinterpret_cast<const char *>(m_data.data()),
              sizeof(uint16) * m_width * m_height);

    ofs.close();

    return true;
}

void VolumeImage::clear(uint16 val)
{
    for (uint32 z = 0; z < m_depth; ++z) {
        for (uint32 y = 0; y < m_height; ++y) {
            for (uint32 x = 0; x < m_width; ++x) {
                operator()(x, y, z) = val;
            }
        }
    }
}

uint16 VolumeImage::value(uint32 x, uint32 y, uint32 z) const
{
    return operator()(x, y, z);
}

void VolumeImage::setValue(uint32 x, uint32 y, uint32 z, uint16 val)
{
    operator()(x, y, z) = val;
}

const uint16 VolumeImage::operator()(uint32 x, uint32 y, uint32 z) const
{
    assert (0 <= x && x < m_width);
    assert (0 <= y && y < m_height);
    assert (0 <= z && z < m_depth);

    const uint32 flippedY = m_height - 1 - y;

    return m_data[x + flippedY * m_width + z * m_width * m_height];
}

uint16 &VolumeImage::operator()(uint32 x, uint32 y, uint32 z)
{
    assert (0 <= x && x < m_width);
    assert (0 <= y && y < m_height);
    assert (0 <= z && z < m_depth);

    const uint32 flippedY = m_height - 1 - y;

    return m_data[x + flippedY * m_width + z * m_width * m_height];
}

} // namespace epw3dimag
