#ifndef H_EPW3DIMAG_VOLUMEIMAGE_H
#define H_EPW3DIMAG_VOLUMEIMAGE_H

#include <string>
#include <vector>

#include "epw3dimag_global.h"

namespace epw3dimag {

class VolumeImage
{
public:
    VolumeImage();
    VolumeImage(uint32 w, uint32 h, uint32 d);
    VolumeImage(const std::string &filename, uint32 w, uint32 h, uint32 d);
    ~VolumeImage();

    std::vector<uint16> data() const;

    uint32 width() const;
    uint32 height() const;
    uint32 depth() const;

    bool isNull() const;
    bool isValid() const;

    uint32 voxelCount() const;

    bool read(const std::string &filename, uint32 w, uint32 h, uint32 d);
    bool write(const std::string &filename) const;

    void clear(uint16 val);

    uint16 value(uint32 x, uint32 y, uint32 z) const;
    void setValue(uint32 x, uint32 y, uint32 z, uint16 val);

    const uint16 operator()(uint32 x, uint32 y, uint32 z) const;
    uint16 &operator()(uint32 x, uint32 y, uint32 z);

private:
    uint32 m_width;
    uint32 m_height;
    uint32 m_depth;

    uint32 m_count;

    std::vector<uint16> m_data;
};

} // namespace epw3dimag

#endif // H_EPW3DIMAG_VOLUMEIMAGE_H
