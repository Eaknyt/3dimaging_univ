#include "volumerenderer.h"

#include <assert.h>
#include <limits>

namespace {

void renderMIP(const epw3dimag::VolumeImage &input,
               epw3dimag::VolumeImage &result)
{
    assert (result.depth() == 1);

    for (std::size_t x = 0; x < input.width(); ++x) {
        for (std::size_t y = 0; y < input.height(); ++y) {
            unsigned short max = 0;

            for (std::size_t z = 0; z < input.depth(); ++z) {
                const unsigned short value = input(x, y, z);

                if (value > max) {
                    max = value;
                }
            }

            result(x, y, 0) = max;
        }
    }
}

void renderAIP(const epw3dimag::VolumeImage &input,
               epw3dimag::VolumeImage &result)
{
    assert (result.depth() == 1);

    const epw3dimag::uint32 inputDepth = input.depth();

    for (std::size_t x = 0; x < input.width(); ++x) {
        for (std::size_t y = 0; y < input.height(); ++y) {
            unsigned short sum = 0;

            for (std::size_t z = 0; z < inputDepth; ++z) {
                const unsigned short value = input(x, y, z);

                sum += value;
            }

            result(x, y, 0) = sum / inputDepth;
        }
    }
}

void renderMinIP(const epw3dimag::VolumeImage &input,
                 epw3dimag::VolumeImage &result)
{
    assert (result.depth() == 1);

    for (std::size_t x = 0; x < input.width(); ++x) {
        for (std::size_t y = 0; y < input.height(); ++y) {
            unsigned short min = std::numeric_limits<unsigned short>::max();

            for (std::size_t z = 0; z < input.depth(); ++z) {
                const unsigned short value = input(x, y, z);

                if (value < min) {
                    min = value;
                }
            }

            result(x, y, 0) = min;
        }
    }
}

} // anon namespace


namespace epw3dimag {

VolumeImage renderVolume(const VolumeImage &img, VolumeRenderMode renderMode)
{
    VolumeImage ret(img.width(), img.height(), 1);

    switch (renderMode) {
    case VolumeRenderMode::MIP:
        renderMIP(img, ret);
        break;
    case VolumeRenderMode::AIP:
        renderAIP(img, ret);
        break;
    case VolumeRenderMode::MinIP:
        renderMinIP(img, ret);
        break;
    default:
        break;
    }

    return ret;
}

} // namespace epw3dimag
