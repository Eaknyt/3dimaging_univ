#ifndef H_EPW3DIMAG_VOLUMERENDERER_H
#define H_EPW3DIMAG_VOLUMERENDERER_H

#include "volumeimage.h"

namespace epw3dimag {

enum class VolumeRenderMode : int
{
    MIP = 1,
    AIP,
    MinIP
};

VolumeImage renderVolume(const VolumeImage &img, VolumeRenderMode renderMode);

} // namespace epw3dimag

#endif // H_EPW3DIMAG_VOLUMERENDERER_H
