#include <algorithm>
#include <iostream>
#include <vector>

#include "epw3dimag/volumeimage.h"


using namespace epw3dimag;


/**
  * Read a raw volume image and print the minimum intensity, the maximum and
  * the intensity of the center voxel.
  *
  * Usage : <inputImgPath> <dimX> <dimY> <dimZ>
*/


int main(int argc, char **argv)
{
    if (argc != 5) {
        std::cout << "Usage : <inputImgPath> <dimX> <dimY> <dimZ>" << std::endl;
        return 1;
    }

    // Read arguments
    const std::string inputImgPath(argv[1]);
    const auto width(static_cast<uint32>(std::stol(argv[2])));
    const auto height(static_cast<uint32>(std::stol(argv[3])));
    const auto depth(static_cast<uint32>(std::stol(argv[4])));

    // Retrieve input image data
    VolumeImage inputImg;

    if (!inputImg.read(inputImgPath, width, height, depth)) {
        std::cerr << "Invalid image path or problem while reading." << std::endl;
        return 1;
    }

    std::vector<uint16> imgData = inputImg.data();

    // Print min and max intensities
    const uint16 minVoxel = *std::min_element(imgData.begin(), imgData.end());
    const uint16 maxVoxel = *std::max_element(imgData.begin(), imgData.end());

    std::cout << "Min voxel > " << minVoxel << std::endl;
    std::cout << "Max voxel > " << maxVoxel << std::endl;

    // Print intensity of the center voxel
    const uint32 cx = width / 2;
    const uint32 cy = height / 2;
    const uint32 cz = depth / 2;

    const uint16 centerVoxel = inputImg(cx, cy, cz);

    std::cout << "Center voxel > " << centerVoxel << std::endl;

    return 0;
}
