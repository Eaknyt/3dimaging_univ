#include <iostream>

#include "epw3dimag/volumeimage.h"
#include "epw3dimag/volumerenderer.h"


using namespace epw3dimag;


/**
  * Read a raw volume image and do a volume rendering to a file.
  *
  * Usage : <inputImgPath> <dimX> <dimY> <dimZ> <outputImgPath> <renderMode>
  *
  * The render mode argument can take the following values :
  * 1 -> Maximum Intensity Projection
  * 2 -> Average Intensity Projection
  * 3 -> Minimum Intensity Projection
*/


int main(int argc, char **argv)
{
    if (argc != 7) {
        std::cout << "Usage : <inputImgPath> <dimX> <dimY> <dimZ>"
                     "<outputImgPath> <renderMode>" << std::endl;
        return 1;
    }

    // Read arguments
    const std::string inputImgPath(argv[1]);
    const auto width(static_cast<uint32>(std::stol(argv[2])));
    const auto height(static_cast<uint32>(std::stol(argv[3])));
    const auto depth(static_cast<uint32>(std::stol(argv[4])));
    const std::string outputImgPath(argv[5]);
    const int renderModeId = std::stoi(argv[6]);

    // Retrieve input image data
    VolumeImage inputImg;

    if (!inputImg.read(inputImgPath, width, height, depth)) {
        std::cerr << "Invalid image path or problem while reading." << std::endl;
        return 1;
    }

    auto renderMode = static_cast<VolumeRenderMode>(renderModeId);

    VolumeImage outputImg = renderVolume(inputImg, renderMode);
    outputImg.write(outputImgPath);

    return 0;
}
